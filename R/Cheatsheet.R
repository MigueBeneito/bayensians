#' Acceso a cheatsheets sobre uso de R y programas afines
#'
#' La funci�n abre para su consulta el pdf del cheatsheet que se indique 
#'
#' @param cheat Argumento que controla el cheatsheet que queramos abrir. Puede tomar como argumento cualquiera de los siguientes valores: \code{advancedr}, \code{baser}, \code{datatable}, \code{datawrangling}, \code{devtools}, \code{ggplot2}, \code{github}, \code{referencermarkdown}, \code{rmarkdown}, \code{rstudio}, \code{shiny}, \code{style}. Este argumento admite cualquier combinaci�n de may�sculas y min�sculas en su valor, incluso subcadenas del argumento que identifiquen su valor de forma un�voca. Todos los dodumentos accesibles por esta funci�n son cheatsheets salvo "style" que es el documento de reglas de estilo de Google para c�digo en R.
#' 
#' Los cheatsheets que corresponden a cada uno de los argumentos de la funci�n son los siguientes:
#' \itemize{
#'  \item \code{advancedr}: Cheatsheet sobre uso avanzado de R
#'  \item \code{baser}: Cheatsheet sobre uso b�sico de R
#'  \item \code{datatable}: Cheatsheet sobre el uso de data.tables
#'  \item \code{datawrangling}: Cheatsheet de R studio sobre las librer�as dplyr y tidyr
#'  \item \code{devtools}: Cheatsheet de R studio sobre developer tools (para hacer paquetes en R)
#'  \item \code{ggplot2}: Cheatsheet de R studio sobre ggplot2
#'  \item \code{github}: Cheatsheet de Github sobre Git
#'  \item \code{referencermarkdown}: Cheatsheet de R studio sobre su entorno de desarrollo (IDE) para R
#'  \item \code{rmarkdown}: Cheatsheet de R studio sobre Rmarkdown
#'  \item \code{rstudio}: Cheatsheet de R studio sobre su entorno de desarrollo (IDE) para R
#'  \item \code{shiny}: Cheatsheet de R studio sobre shiny
#'  \item \code{style}: Documento de estilo de Google sobre la redacci�n de c�gigo en R
#' }
#' @return No devuelve ning�n valor, simplemente abre el cheatsheet correspondiente para su consulta
#' @examples
#' CheatSheet("shiny")
#' CheatSheet("Git")
#' @export
CheatSheet<-function(cheat){
  if(is.character(cheat)){
    cheat=tolower(cheat)
  }
  else{
    stop("cheat is not an argument of class character")
  }
  files<-c("advancedr","baser","datatable","datawrangling","devtools","ggplot2","github","referencermarkdown","rmarkdown","rstudio","shiny","style")
  thisFile<-charmatch(cheat,files)
  if(thisFile==0){stop("cheat argument coincides with several options")}
  if(is.na(thisFile)){stop("cheat argument takes an unrecognized argument")}
  path<-normalizePath(paste0("vignettes/",files[thisFile],".pdf"))
  system(paste0("open ", path))
  return(NULL)
}
